### Package Challenge

According to the task constraints, I've used solution with complexity of O(2^n) where n - number of items.
Because we have up to 15 items in our package and weight with floating point numbers with arbitrary precision.

    e.g. 
         
    100 - weight limit
    100 - max weight and cost of items
    15  - max items
    
    O(2^n) = 2^15 = 32_768

---    
But in case we have weight with floating point numbers with fixed precision of 2 items and items will be increased. 
Then it's better to use dynamic approach with complexity of O(nw), where n - number of items and w - max package weight limit.

    e.g. 
         
    100 - max weight limit
    100 - max weight and cost of items
    25  - max items
    
    O(2^n) = 2^25 = 33_554_432
    O(Wn) = (100 * 100) * 25 = 250_000
     
- n - number of items in the package
- w - max wight limit



#### used dependencies:

- lombok - for removing boilerplate code
- appache-commons - for some usefull work with collections and io 
- juint and assertj - for testing