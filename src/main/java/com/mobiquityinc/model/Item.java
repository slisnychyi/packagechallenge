package com.mobiquityinc.model;

import lombok.Data;

@Data
public class Item {

    private final int id;
    private final double weight;
    private final int price;

}
