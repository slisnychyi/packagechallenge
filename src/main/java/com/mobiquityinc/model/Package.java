package com.mobiquityinc.model;

import lombok.Data;

import java.util.List;

@Data
public class Package {

    private final int weightLimit;
    private final List<Item> items;

}
