package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.reader.ItemFileReader;
import com.mobiquityinc.services.DefaultItemsSelector;
import com.mobiquityinc.services.ItemsSelector;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class Packer {

    private final static ItemFileReader READER = new ItemFileReader();

    private Packer() {
    }

    public static String pack(String filePath) throws APIException {
        if (isBlank(filePath)) {
            throw new APIException(format("wrong filePath[%s] for packaging.", filePath));
        }
        List<Package> packages = READER.readPackageCases(filePath);
        return packages.stream()
                .map(pack -> new DefaultItemsSelector(pack.getItems(), pack.getWeightLimit()))
                .map(ItemsSelector::getSelectedItems)
                .collect(Collectors.joining("\n"));
    }
}
