package com.mobiquityinc.reader;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.model.Item;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.*;
import static java.lang.String.*;
import static java.nio.file.Files.*;
import static java.util.Arrays.*;

public class ItemFileReader {

    private static final String CURRENCY_ITEM = "€";
    private static final String ROW_VALIDATION_PATTERN = "^([1-9][0-9]?|100)\\s?[:]\\s?([(]([1-9]|1[0-5])[,]([1-9][0-9]?|100)[.]([0-9]{1,2})[,][€]([1-9][0-9]?|100)[)]\\s?){1,15}$";

    @SneakyThrows
    public List<Package> readPackageCases(String filePath) {
        List<Package> result = new ArrayList<>();
        List<String> items = readLines(filePath);
        for (String item : items) {
            if (!item.matches(ROW_VALIDATION_PATTERN)) {
                throw new APIException(format("invalid format of the item[%s]", item));
            }
            int pointsIndex = item.indexOf(":");
            int weightLimit = parseInt(item.substring(0, pointsIndex).trim());
            List<Item> packageItems = stream(item.substring(pointsIndex + 1).trim().split(" "))
                    .map(e -> e.substring(1, e.length() - 1))
                    .map(e -> e.replaceAll(CURRENCY_ITEM, ""))
                    .map(e -> {
                        String[] values = e.split(",");
                        return new Item(valueOf(values[0]), Double.valueOf(values[1]), valueOf(values[2]));
                    })
                    .filter(e -> e.getWeight() <= weightLimit)
                    .collect(Collectors.toList());
            result.add(new Package(weightLimit, packageItems));
        }
        return result;
    }

    List<String> readLines(String filePath) throws APIException {
        try {
            return readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            throw new APIException(e.getMessage(), e);
        }
    }
}
