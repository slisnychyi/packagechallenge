package com.mobiquityinc.services;

import com.mobiquityinc.model.Item;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.of;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;

public class DefaultItemsSelector implements ItemsSelector {

    private final List<Item> items;
    private final int limit;

    public DefaultItemsSelector(List<Item> items, int limit) {
        this.items = items;
        this.limit = limit;
    }

    @Override
    public String getSelectedItems() {
        return of(getItems(new ArrayList<>(), 0, 0))
                .filter(CollectionUtils::isNotEmpty)
                .map(items -> items.stream()
                        .map(Item::getId)
                        .map(String::valueOf)
                        .collect(Collectors.joining(",")))
                .orElse("-");
    }

    private List<Item> getItems(List<Item> selected, double totalWeight, int pointer) {
        if (pointer >= items.size()) {
            return selected;
        }
        Item e = items.get(pointer);
        List<Item> second = new ArrayList<>();
        int level = ++pointer;
        if (totalWeight + e.getWeight() < limit) {
            List<Item> selectedCopy = new ArrayList<>(selected);
            selectedCopy.add(e);
            second = getItems(selectedCopy, totalWeight + e.getWeight(), level);
        }
        List<Item> first = getItems(selected, totalWeight, level);
        return getGreaterItems(first, second);
    }

    private List<Item> getGreaterItems(List<Item> first, List<Item> second) {
        Pair<Integer, Double> totalFirst = getTotal(first);
        Pair<Integer, Double> totalSecond = getTotal(second);
        if (allNotNull(totalFirst, totalSecond)) {
            if (totalFirst.getLeft().equals(totalSecond.getLeft())) {
                return totalFirst.getRight() < totalSecond.getRight() ? first : second;
            } else {
                return totalFirst.getLeft() > totalSecond.getLeft() ? first : second;
            }

        }
        return totalFirst != null ? first : totalSecond != null ? second : null;
    }

    private Pair<Integer, Double> getTotal(List<Item> items) {
        return items
                .stream()
                .map(i -> Pair.of(i.getPrice(), i.getWeight()))
                .reduce((first, second) ->
                        Pair.of(first.getLeft() + second.getLeft(), first.getRight() + second.getRight()))
                .orElse(null);
    }
}