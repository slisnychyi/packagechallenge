package com.mobiquityinc.services;

public interface ItemsSelector {

    String getSelectedItems();

}
