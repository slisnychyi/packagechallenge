package com.mobiquityinc.reader;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.model.Item;
import org.apache.commons.io.IOUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static java.util.Arrays.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;


public class ItemFileReaderTest {

    @Test(expected = APIException.class)
    public void should_throwException_when_wrongFilePath() {
        //given
        ItemFileReader reader = new ItemFileReader();
        //when
        reader.readPackageCases("file_path");
    }

    @Test(expected = APIException.class)
    public void should_throwException_when_invalidData() throws APIException, IOException {
        //given
        ItemFileReader reader = spy(new ItemFileReader());
        String filePath = "/items_invalid.txt";
        List<String> items = IOUtils.readLines(this.getClass().getResourceAsStream(filePath), Charset.defaultCharset());
        doReturn(items).when(reader).readLines(filePath);
        //when
        reader.readPackageCases(filePath);
        //then
    }

    @Test
    public void should_getItems_when_validFileData() throws APIException, IOException {
        //given
        ItemFileReader reader = spy(new ItemFileReader());
        String filePath = "/items_valid.txt";
        List<String> items = IOUtils.readLines(this.getClass().getResourceAsStream(filePath), Charset.defaultCharset());
        doReturn(items).when(reader).readLines(filePath);
        //when
        List<Package> result = reader.readPackageCases(filePath);
        //then
        List<Package> expected = asList(
                new Package(81, asList(
                        new Item(1, 53.38, 45),
                        new Item(3, 78.48, 3),
                        new Item(4, 72.30, 76),
                        new Item(5, 30.18, 9),
                        new Item(6, 46.34, 48))),
                new Package(8, Lists.emptyList()),
                new Package(75, asList(
                        new Item(2, 14.55, 74),
                        new Item(3, 3.98, 16),
                        new Item(4, 26.24, 55),
                        new Item(5, 63.69, 52),
                        new Item(7, 60.02, 74))),
                new Package(56, asList(
                        new Item(2, 33.80, 40),
                        new Item(3, 43.15, 10),
                        new Item(4, 37.97, 16),
                        new Item(5, 46.81, 36),
                        new Item(6, 48.77, 79),
                        new Item(8, 19.36, 79),
                        new Item(9, 6.76, 64)
                ))
        );
        assertThat(result).isEqualTo(expected);
    }


}