package com.mobiquityinc.services;

import com.mobiquityinc.model.Item;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultItemsSelectorTest {

    @Test
    public void should_return_emptyResult_when_noItemsExist() {
        //given
        DefaultItemsSelector itemsSelector = new DefaultItemsSelector(new ArrayList<>(), 10);
        //when
        String result = itemsSelector.getSelectedItems();
        //then
        assertThat(result).isEqualTo("-");
    }

    @Test
    public void should_selectItemsForPackage_when_valuesOnCloseTreeLevels() {
        //given
        int limit = 56;
        List<Item> items = Stream.of(
                new Item(1, 90.72, 13),
                new Item(2, 33.80, 40),
                new Item(3, 43.15, 10),
                new Item(4, 37.97, 16),
                new Item(5, 46.81, 36),
                new Item(6, 48.77, 79),
                new Item(7, 81.80, 45),
                new Item(8, 19.36, 79),
                new Item(9, 6.76, 64))
                .filter(e -> e.getWeight() <= limit)
                .collect(Collectors.toList());
        DefaultItemsSelector itemsSelector = new DefaultItemsSelector(items, limit);
        //when
        String result = itemsSelector.getSelectedItems();
        //then
        assertThat(result).isEqualTo("8,9");
    }

    @Test
    public void should_selectBestItemsForPackage_when_valuesOnDifferentTreeLevels() {
        //given
        int limit = 75;
        List<Item> items = Stream.of(
                new Item(1, 85.31, 29),
                new Item(2, 14.55, 74),
                new Item(3, 3.98, 16),
                new Item(4, 26.24, 55),
                new Item(5, 63.69, 52),
                new Item(6, 76.25, 75),
                new Item(7, 60.02, 74),
                new Item(8, 93.18, 35),
                new Item(9, 89.95, 78))
                .filter(e -> e.getWeight() <= limit)
                .collect(Collectors.toList());
        DefaultItemsSelector itemsSelector = new DefaultItemsSelector(items, limit);
        //when
        String result = itemsSelector.getSelectedItems();
        //then
        assertThat(result).isEqualTo("2,7");
    }

    @Test
    public void should_selectNoItems_when_itemsWeightMoreThanLimit() {
        //given
        int limit = 8;
        List<Item> items = Stream.of(new Item(1, 15.31, 29))
                .filter(e -> e.getWeight() <= limit)
                .collect(Collectors.toList());
        DefaultItemsSelector itemsSelector = new DefaultItemsSelector(items, limit);
        //when
        String result = itemsSelector.getSelectedItems();
        //then
        assertThat(result).isEqualTo("-");
    }

    @Test
    public void should_selectItem_when_existOneMostValuable() {
        //given
        int limit = 81;
        List<Item> items = Stream.of(
                new Item(1, 53.38, 45),
                new Item(2, 88.62, 98),
                new Item(3, 78.48, 3),
                new Item(4, 72.30, 76),
                new Item(5, 30.18, 9),
                new Item(6, 46.34, 48))
                .filter(e -> e.getWeight() <= limit)
                .collect(Collectors.toList());
        DefaultItemsSelector itemsSelector = new DefaultItemsSelector(items, limit);
        //when
        String result = itemsSelector.getSelectedItems();
        //then
        assertThat(result).isEqualTo("4");
    }
}